import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import Button from './components/Button';
import Heading from './components/Heading';
import Input from './components/Input';
import List from './components/List';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      items: [],
      userInput: '',
    };
  }

  getInput = userInput => {
    this.setState({
      userInput: userInput,
    });
  };

  add = () => {
    this.setState({
      items: [this.state.userInput, ...this.state.items],
      userInput: '',
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Heading />
        <Input getInput={this.getInput} value={this.state.userInput} />
        <List items={this.state.items} />
        <Button add={this.add} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    paddingBottom: 50,
  },
});
