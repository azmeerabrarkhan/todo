import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Item from './Item';

const List = props => {
  console.log(props);
  getItem = item => {
    return <Item item={item} />;
  };
  return <ScrollView>{props.items.map(getItem)}</ScrollView>;
};

export default List;

const styles = StyleSheet.create({});
