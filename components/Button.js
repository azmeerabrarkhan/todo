import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const Button = props => {
  return (
    <TouchableOpacity onPress={props.add} style={styles.button}>
      <Text style={styles.buttonText}>Add</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  button: {
    marginTop: 15,
    backgroundColor: '#fff',
    height: 60,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    shadowRadius: 3,
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
});
