import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Heading = () => {
  return (
    <View>
      <Text style={styles.heading}>Todos</Text>
    </View>
  );
};

export default Heading;

const styles = StyleSheet.create({
  heading: {
    marginTop: 70,
    fontSize: 70,
    fontWeight: '100',
    textAlign: 'center',
    color: '#ad2f2f',
  },
});
