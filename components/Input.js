import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

const Input = props => {
  onChangeTextHandler = userInput => {
    props.getInput(userInput);
  };
  return (
    <View>
      <TextInput
        onChangeText={onChangeTextHandler}
        value={props.value}
        style={styles.input}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: {
    marginTop: 15,
    backgroundColor: '#fff',
    height: 60,
    fontSize: 17,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    shadowRadius: 3,
  },
});
