import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const Item = props => {
  return (
    <View style={styles.item}>
      <Text>{props.item}</Text>
      <TouchableOpacity style={styles.delete}>
        <Text style={styles.deleteText}>Delete</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Item;

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#fff',
    height: 40,
    fontSize: 17,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    shadowRadius: 3,
    borderWidth: 1,
    borderBottomWidth: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: '#e3e3e3',
  },
  delete: {borderWidth: 1, borderColor: '#e3e3e3', padding: 4, borderRadius: 6},
  deleteText: {color: '#ad2f2f'},
});
